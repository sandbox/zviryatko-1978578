<?php
/**
 * @file
 * Commerce JCC payment pages.
 */

/**
 * Return response code description.
 *
 * Information from DeveloperGuidev9.7.pdf, 'Appendix A – Response Codes'
 *
 * @param int $responce_code
 *   Responce code form $_POST['ResponseCode']
 *
 * @return string
 *   Response code description
 */
function jcc_get_responce_code_description($responce_code) {
  $responce_description = array(
    0 => t('Token/hash deactivation'),
    1 => t('Approved'),
    2 => t('Declined'),
  );
  return isset($responce_description[$responce_code]) ? $responce_description[$responce_code] : NULL;
}

/**
 * Return reason code description from code.
 *
 * Information from DeveloperGuidev9.7.pdf, 'Appendix B - Reason Codes'
 *
 * @param int $reason_code
 *   Reason code from $_POST['ReasonCode']
 *
 * @return string
 *   Reason code description
 */
function jcc_get_reason_code_description($reason_code) {
  $reason_description = array(
    1 => t('Approved'),
    2 => t('Declined'),
    6 => t('Connection was expired due to timeout in the checkout'),
    7 => t('Field is Missing'),
    8 => t('Field Format is Invalid'),
    10 => t('No Such Merchant'),
    11 => t('Authentication Failed (Signature is wrong)'),
    12 => t('Merchant is not enabled for processing'),
    14 => t('Currency is not allowed for the specific merchant'),
    15 => t('Invalid Connection Method'),
    22 => t('Invalide Vres data from ACS'),
    41 => t('Session Expired'),
    90 => t('System Malfunction'),
    96 => t('Transaction was cancelled by the cardholder.'),
    150 => t('Duplicate Order Id'),
    500 => t('Card is expired'),
    501 => t('Currency Is Not Defined'),
    502 => t('Capture Mode is Invalid'),
    503 => t('Approved but not Captured'),
    504 => t('Unknown Card Network'),
    505 => t('Fraud Control Failed'),
    506 => t('Address is not defined'),
    507 => t('City is not Defined'),
    508 => t('Unknown Order id'),
    509 => t('Unknown Response'),
    510 => t('Host Connection Problem'),
    511 => t('ISO Message cannot be created'),
    512 => t('Error - Cardholder authentication failure'),
    513 => t('3D Secure Message does not exist'),
    514 => t('Invalid Amount'),
    515 => t('Amount is not allowed'),
    516 => t('Operation is not allowed'),
    517 => t('Capture is expired'),
    518 => t('Refund is expired'),
    519 => t('Reversal is expired'),
    520 => t('Invalid Https response URL'),
    601 => t('Merchant is not set to use the direct method.'),
    602 => t('Merchant is not set to use the redirect method.'),
    603 => t('Merchant is not set to use the server method.'),
    604 => t('Merchant is not set to use the Moto method.'),
    606 => t('Communication failure'),
    610 => t('Unable To Authenticate'),
    611 => t('Customer failed or cancelled authentication. Transaction denied.'),
    612 => t('Communication with DS Failure'),
    613 => t('Authentication could not be completed due to technical or other problems'),
    614 => t('Required element missing'),
    615 => t('Cardholder authentication failure'),
    616 => t('Invalid Card Number'),
    617 => t('Card Not defined In system'),
    618 => t('Hash Service not Allowed For Merchant'),
    619 => t('CVV2 Not Present'),
    620 => t('Advanced Authorization Message Not Allowed'),
    621 => t('Global Tokenization Service not Allowed For Merchant'),
    622 => t('Merchant is not enabled for Dynamic Descriptor'),
    642 => t('CVV2 length is invalid (3 digits only)'),
    899 => t('Fraud Control Decline'),
  );
  return isset($reason_description[$reason_code]) ? $reason_description[$reason_code] : NULL;
}

/**
 * Show commerce JCC payment status code & status message.
 *
 * @return null|string
 */
function commerce_jcc_result_page() {
  if (isset($_POST['OrderID']) && $order = commerce_order_load((int) $_POST['OrderID'])) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $transaction = commerce_payment_transaction_new('jcc', $order->order_id);

    // todo: status & message dont set =(
    if (!isset($_POST['ResponseCode']) && !isset($_POST['ReasonCode'])) {
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      drupal_set_message('No POST');
      $transaction->message = t('No POST data received');
    } elseif ($_POST['ResponseCode'] == 1 && $_POST['ReasonCode'] == 1) {
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('Payment succeeded with message: %message', array('%message' => $_POST['ReasonCodeDesc']));
    } else {
      // todo: create reason codes list and return them from pdf Appendix B
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $msg = t('Payment failed with code %code and message: %message', array('%code' => $_POST['ReasonCode'], '%message' => $_POST['ReasonCodeDesc']));;
      $transaction->message = $msg;
      commerce_payment_redirect_pane_previous_page($order);
      return $msg;
    }

    $amount = $wrapper->commerce_order_total->amount->value();
    $currency = $wrapper->commerce_order_total->currency_code->value();
    $transaction->amount = $amount;
    $transaction->currency_code = $currency;

    commerce_payment_transaction_save($transaction);

    commerce_payment_redirect_pane_next_page($order);
    watchdog('commerce_paypal_jcc', 'IPN processed for Order @order_number with @status.', array('@order_number' => $order->order_number, '@status' => $_POST['ReasonCodeDesc']), WATCHDOG_INFO);

    $output = '';
    if (isset($_POST['ResponseCode'])) {
      $output .= t('Payment failed with code %code and message: %message', array('%code' => $_POST['ReasonCode'], '%message' => $_POST['ReasonCodeDesc']));
    }
    return $output;
  }
  drupal_not_found();
  return NULL;
}